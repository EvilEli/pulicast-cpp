/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef INCLUDE_PULICAST_CORE_SUBSCRIPTION_DISPATCHER_H_
#define INCLUDE_PULICAST_CORE_SUBSCRIPTION_DISPATCHER_H_

#include <functional>

#include <asio/buffer.hpp>

#include "pulicast/address.h"
#include "pulicast/message_info.h"


namespace pulicast::core {

using SubscriptionCallback = std::function<void(const char *, size_t, const MessageInfo&)>;

class SubscriptionDispatcher {

 public:
  void Dispatch(const asio::const_buffer& packet, Address source, int lead) {
    // TODO(max): by this construction we can not subscribe inside callbacks since that would
    //  invalidate the iterator! Ref issue #30
    auto message_info = MessageInfo{source, lead};

    for (auto &callback : subscriptions_) {
      callback(asio::buffer_cast<const char *>(packet), asio::buffer_size(packet), message_info);
    }
  }

  void Subscribe(SubscriptionCallback callback) {
    subscriptions_.push_back(std::move(callback));
  }

 private:
  std::vector<SubscriptionCallback> subscriptions_;

};

} // pulicast::core

#endif //INCLUDE_PULICAST_CORE_SUBSCRIPTION_DISPATCHER_H_
