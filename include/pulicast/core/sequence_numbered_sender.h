/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef INCLUDE_PULICAST_CORE_SEQUENCE_NUMBERED_SENDER_H_
#define INCLUDE_PULICAST_CORE_SEQUENCE_NUMBERED_SENDER_H_
#include <atomic>

#include "pulicast/core/transport.h"

namespace pulicast::core {

class SequenceNumberedSender {
 public:
  SequenceNumberedSender(std::unique_ptr<core::ChannelSender> channel_sender)
  : seq_no_(0), channel_sender_(std::move(channel_sender)) {}

  SequenceNumberedSender() = delete;
  SequenceNumberedSender(const SequenceNumberedSender &) = delete;
  SequenceNumberedSender &operator=(const SequenceNumberedSender &) = delete;
  SequenceNumberedSender(SequenceNumberedSender &&other) = delete;
  SequenceNumberedSender &operator=(SequenceNumberedSender &&) = delete;
  ~SequenceNumberedSender() = default;

  void Send(ScatterGatherBuffer data) {
    uint32_t seq_no_nbo_ = htonl(seq_no_++);
    auto data_with_seqno = data.push_front({reinterpret_cast<char *>(&seq_no_nbo_),
                                            sizeof(seq_no_nbo_)});
    channel_sender_->Send(data_with_seqno);
  }

  uint32_t GetSequenceNumber() const {
    return seq_no_;
  }

 private:
  std::atomic<uint32_t> seq_no_;
  std::unique_ptr<core::ChannelSender> channel_sender_;
};

}  // namsepace pulicast::core

#endif //INCLUDE_PULICAST_CORE_SEQUENCE_NUMBERED_SENDER_H_
