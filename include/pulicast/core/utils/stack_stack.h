/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef INCLUDE_PULICAST_CORE_UTILS_STACK_STACK_H_
#define INCLUDE_PULICAST_CORE_UTILS_STACK_STACK_H_

namespace pulicast::core {

/**
 * @brief A stack data structure, that stores its values on the call-stack.
 *
 * This allows us to build up a stack data structure while descending the call-stack. This is
 * especially useful when constructing a scatter-gather buffer for a network packet. The different
 * hierarchies of the call-stack represent the different layers of the network protocol.
 *
 * @tparam T The type of value to store in the stack.
 */
template<class T>
struct StackStack {
  using value_type = T;
  const T data;
  const StackStack *next = nullptr;

  template<class ValueT>
  struct stack_iterator {
    using value_type = ValueT;
    using pointer = const value_type *;
    using reference = const value_type &;
    using iterator_category = std::input_iterator_tag;
    using difference_type = void;

    stack_iterator &operator++() {
      ptr = ptr->next;
      return *this;
    }
    bool operator==(stack_iterator other) const {
      return ptr == other.ptr;
    }
    bool operator!=(stack_iterator other) const {
      return !(*this == other);
    }
    reference operator*() { return ptr->data; }

    const StackStack *ptr;
  };

  using iterator = stack_iterator<T>;
  using const_iterator = stack_iterator<const T>;

  const_iterator begin() const { return const_iterator{this}; }
  const_iterator end() const { return const_iterator{nullptr}; }

  /**
   * @brief Adds a new value to the front of the stack data structure.
   *
   * Note: the return type of this function is the new state of this stack and it must not be
   * discarded! You have to continue working with this stack on the basis of the new state.
   * Consider this example:
   *
   * \code{.cpp}
   * auto s1 = StackStack<int>{1};
   * auto s2 = s1.push_front(1);
   * auto s3 = s2.push_front(2);
   *
   * OtherFunctionExtendingTheStack(s3);
   * \endcode
   *
   * @param val The value to add to the stack.
   * @return The new state of the stack with which to continue stack construction.
   * Do not use the old state any more!
   */
  [[nodiscard]] StackStack push_front(T val) const {
    return {val, this};
  }
};

}  // namespace pulicast::core

#endif //INCLUDE_PULICAST_CORE_UTILS_STACK_STACK_H_
