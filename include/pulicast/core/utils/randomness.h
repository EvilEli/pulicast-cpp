/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CPP_INCLUDE_PULICAST_CORE_UTILS_RANDOMNESS_H_
#define CPP_INCLUDE_PULICAST_CORE_UTILS_RANDOMNESS_H_

#ifdef CUSTOM_RAND
#include CUSTOM_RAND
#else

#include <random>

namespace pulicast::core {
  inline int64_t pulicast_rand() {
    auto engine = std::default_random_engine(std::random_device{}());
    auto distribution = std::uniform_int_distribution<int64_t>{};
    return distribution(engine);
  }
}
#endif

#endif //CPP_INCLUDE_PULICAST_CORE_UTILS_RANDOMNESS_H_
