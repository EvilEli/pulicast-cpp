/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef INCLUDE_PULICAST_CORE_TRANSPORT_H_
#define INCLUDE_PULICAST_CORE_TRANSPORT_H_

#include <functional>
#include <memory>
#include <string>

#include <asio/buffer.hpp>

#include "pulicast/address.h"
#include "pulicast/core/utils/scatter_gather_buffer.h"

namespace pulicast::core {

/**
 * @brief Abstract base class for 'send'-functors that send on a specific pulicast channel.
 */
class ChannelSender {
 public:
  virtual ~ChannelSender() = default;
  virtual void Send(ScatterGatherBuffer& data) = 0;
};

/**
 * @brief Abstract base class for a transport to be used by pulicast channels to send and receive
 * packets.
 */
class Transport {
 public:
  virtual ~Transport() = default;

  /**
   * @brief Create a new ChannelSender object to send data on a channel.
   * @param channel_name The name of the channel to send on.
   * @return A unique ptr to a new channel sender object.
   */
  virtual std::unique_ptr<ChannelSender> MakeChannelSender(const std::string &channel_name) = 0;

  /**
   * @brief Starts receiving messages on the given channel and calls the passed callback whenever
   * a new packet arrives.
   *
   * @param channel_name The name of the channel to receive from.
   * @param packet_callback The callback to call when a new packet arrived.
   * It takes an asio mutable buffer and the address of the packet source as parameters.
   */
  virtual void StartReceivingMessagesFor(
      const std::string &channel_name,
      std::function<void(asio::mutable_buffer&, Address)> packet_callback) = 0;
};

}  // namespace pulicast::core

#endif //INCLUDE_PULICAST_CORE_TRANSPORT_H_
