/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CPP_INCLUDE_PULICAST_ZCM_SERIALIZATION_H_
#define CPP_INCLUDE_PULICAST_ZCM_SERIALIZATION_H_

#include <optional>
#include <asio/buffer.hpp>

#include "pulicast_messages/int8_msg.hpp"
#include "pulicast_messages/int16_msg.hpp"
#include "pulicast_messages/int32_msg.hpp"
#include "pulicast_messages/int64_msg.hpp"
#include "pulicast_messages/float_msg.hpp"
#include "pulicast_messages/double_msg.hpp"
#include "pulicast_messages/string_msg.hpp"
#include "pulicast_messages/boolean_msg.hpp"
#include "pulicast_messages/byte_msg.hpp"

#include "pulicast/core/utils/scatter_gather_buffer.h"

namespace pulicast {
/**
 * @brief A shortcut to check if a given type describes a valid encoding callback.
 */
template<class T>
using IsEncodingCallback = std::enable_if<std::is_invocable_v<T, pulicast::core::ScatterGatherBuffer> >;

/**
 * @brief A shortcut to detect ZCM types.
 *
 * This is just a heuristic though that checks if the given type has a encode(), decode() and
 * getEncodedSize() method.
 */
template<class T>
using IsZCMType = std::enable_if<
    std::is_member_function_pointer_v<decltype(&T::encode)> &&
    std::is_member_function_pointer_v<decltype(&T::getEncodedSize)> &&
    std::is_member_function_pointer_v<decltype(&T::decode)> >;

/**
 * @brief Decodes a ZCM message into a provided std::optional.
 *
 * @tparam T The type to decode.  Anything with the methods getEncodedSize(), encode() and decode()
 * is allowed.
 * @tparam WrappingT The pulicast_messages type, that the primitive value is wrapped in.
 * Usually deduced automatically.
 * @param buffer The buffer to decode the value from.
 * @param target The target value to place the decoded value to. This is an output parameter.
 */
template<class T, class = IsZCMType<T> >
inline void DecodeMessageInto(const asio::const_buffer &buffer, std::optional<T> &target) {
  T msg;
  if (msg.decode(asio::buffer_cast<const char *>(buffer), 0, asio::buffer_size(buffer)) < 0) {
    target = std::nullopt;
  } else {
    target = msg;
  }
}

/**
 * @brief Encodes a ZCM message and then calls a callback with the resulting
 * pulicast::core::ScatterGatherBuffer.
 *
 * @tparam T The type of the message to encode. Anything with the methods getEncodedSize(),
 * encode() and decode() is allowed.
 * @tparam CallbackT The type of the callback to call on completion of the encoding. Can be anything
 * invocable with a ScatterGatherBuffer.
 * @param message The value to encode.
 * @param callback The callback to call after completion.
 */
template<class T, class CallbackT, class = IsZCMType<T>, class = IsEncodingCallback<CallbackT> >
inline void EncodeAndThenCall(const T &message, CallbackT &&callback) {
  const size_t kEncodedSize = message.getEncodedSize();
  char encode_buffer[kEncodedSize];  // TODO(max): this is not valid C++, Ref issue #10
  const size_t kMessageSize = message.encode(encode_buffer, 0, sizeof(encode_buffer));
  callback(pulicast::core::ScatterGatherBuffer{asio::buffer(encode_buffer, kMessageSize)});
}


namespace {
template<class WrappingT, class T>
inline void DecodePrimitive(const asio::const_buffer &buffer, std::optional<T> &target) {
  std::optional<WrappingT> wrapping_msg;
  DecodeMessageInto(buffer, wrapping_msg);
  if (wrapping_msg) {
    target = wrapping_msg->value;
  } else {
    target = std::nullopt;
  }
}

template<class WrapperType, class T, class CallbackT>
inline void EncodePrimitiveAndThenCall(const T &message, CallbackT &&callback) {
  auto wrapper_message = WrapperType();
  wrapper_message.value = message;
  EncodeAndThenCall(wrapper_message, std::move(callback));
}
}  // anonymous namespace

/**
 * Template specializations of DecodeMessageInto and EncodeAndThenCall.
 * Consider the README for more information about how to add support for additional message types.
 */

inline void DecodeMessageInto(const asio::const_buffer& buffer, std::optional<int8_t> &target) { DecodePrimitive<pulicast_messages::int8_msg>(buffer, target); };
inline void DecodeMessageInto(const asio::const_buffer& buffer, std::optional<uint8_t> &target) { DecodePrimitive<pulicast_messages::byte_msg>(buffer, target); };
inline void DecodeMessageInto(const asio::const_buffer& buffer, std::optional<int16_t> &target) { DecodePrimitive<pulicast_messages::int16_msg>(buffer, target); };
inline void DecodeMessageInto(const asio::const_buffer& buffer, std::optional<int32_t> &target) { DecodePrimitive<pulicast_messages::int32_msg>(buffer, target); };
inline void DecodeMessageInto(const asio::const_buffer& buffer, std::optional<int64_t> &target) { DecodePrimitive<pulicast_messages::int64_msg>(buffer, target); };
inline void DecodeMessageInto(const asio::const_buffer& buffer, std::optional<float> &target) { DecodePrimitive<pulicast_messages::float_msg>(buffer, target); };
inline void DecodeMessageInto(const asio::const_buffer& buffer, std::optional<double> &target) { DecodePrimitive<pulicast_messages::double_msg>(buffer, target); };
inline void DecodeMessageInto(const asio::const_buffer& buffer, std::optional<std::string> &target) { DecodePrimitive<pulicast_messages::string_msg>(buffer, target); };
inline void DecodeMessageInto(const asio::const_buffer& buffer, std::optional<bool> &target) { DecodePrimitive<pulicast_messages::boolean_msg>(buffer, target); };
// Note: we need this one since the bool implementation binds stronger to a const char * than the
// std::string implementation
template<size_t len>
inline void DecodeMessageInto(const asio::const_buffer& buffer, std::optional<char[len]> &target) { DecodePrimitive<pulicast_messages::string_msg>(buffer, target); };

template<class CallbackT> inline void EncodeAndThenCall(const int8_t& msg, CallbackT &&callback) { EncodePrimitiveAndThenCall<pulicast_messages::int8_msg>(msg, callback); };
template<class CallbackT> inline void EncodeAndThenCall(const uint8_t& msg, CallbackT &&callback) { EncodePrimitiveAndThenCall<pulicast_messages::byte_msg>(msg, callback); };
template<class CallbackT> inline void EncodeAndThenCall(const int16_t& msg, CallbackT &&callback) { EncodePrimitiveAndThenCall<pulicast_messages::int16_msg>(msg, callback); };
template<class CallbackT> inline void EncodeAndThenCall(const int32_t& msg, CallbackT &&callback) { EncodePrimitiveAndThenCall<pulicast_messages::int32_msg>(msg, callback); };
template<class CallbackT> inline void EncodeAndThenCall(const int64_t& msg, CallbackT &&callback) { EncodePrimitiveAndThenCall<pulicast_messages::int64_msg>(msg, callback); };
template<class CallbackT> inline void EncodeAndThenCall(const float& msg, CallbackT &&callback) { EncodePrimitiveAndThenCall<pulicast_messages::float_msg>(msg, callback); };
template<class CallbackT> inline void EncodeAndThenCall(const double& msg, CallbackT &&callback) { EncodePrimitiveAndThenCall<pulicast_messages::double_msg>(msg, callback); };
template<class CallbackT> inline void EncodeAndThenCall(const std::string& msg, CallbackT &&callback) { EncodePrimitiveAndThenCall<pulicast_messages::string_msg>(msg, callback); };
template<class CallbackT, size_t len> inline void EncodeAndThenCall(const char(&msg)[len], CallbackT &&callback) { EncodePrimitiveAndThenCall<pulicast_messages::string_msg>(msg, callback); };
template<class CallbackT> inline void EncodeAndThenCall(const bool& msg, CallbackT &&callback) { EncodePrimitiveAndThenCall<pulicast_messages::boolean_msg>(msg, callback); };

/**
 * @brief Decodes a message of a given type.
 *
 * @note This function just uses a previously defined DecodeMessageInto function to do the actual
 * decoding.
 *
 * @tparam T The type of the message to decode.
 * @param buffer The buffer to decode the message from.
 * @return Either the message or std::nullopt when decoding was not successful.
 */
template<class T>
inline std::optional<T> Decode(const asio::const_buffer &buffer) { // TODO: explain this weird setup
  std::optional<T> ret;
  DecodeMessageInto(buffer, ret);
  return ret;
}
}  // namespace pulicast


#endif //CPP_INCLUDE_PULICAST_ZCM_SERIALIZATION_H_
