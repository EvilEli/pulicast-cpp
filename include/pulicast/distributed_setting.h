/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef INCLUDE_PULICAST_DISTRIBUTED_SETTING_H_
#define INCLUDE_PULICAST_DISTRIBUTED_SETTING_H_

#include <utility>
#include <set>

#include <asio/buffers_iterator.hpp>
#include "pulicast_messages/roc_ack.hpp"
#include "pulicast_messages/poke_setting.hpp"
#include "pulicast_messages/reset_setting_to_default.hpp"
#include "pulicast_messages/unset_setting.hpp"

#include <pulicast/node.h>

namespace pulicast::experimental::distributed_settings {

using namespace std::string_literals;

/**
 * @brief Gets the channel on which requests of change to a distributed setting on this namespace
 * will be made.
 *
 * @param setting_name The name of the distributed setting.
 * @param ns The namespace to which the distributed setting belongs.
 * @return A reference to the channel.
 */
inline auto &ChannelForRequestsOfChange(const std::string &setting_name, Namespace &ns) {
  return (ns / ".config")[setting_name + ".r"];
}

/**
 * @brief Gets the channel on which confirmations of a distributed setting associated with
 * the given ns are sent.
 *
 * @param setting_name The name of the distributed setting.
 * @param ns The namespace to which the distributed setting belongs.
 * @return A reference to the channel.
 */
inline auto &ChannelForConfirmations(const std::string &setting_name, Namespace &ns) {
  return (ns / ".config")[setting_name];
}

/**
 * @brief A function to validate requests of change.
 * Should return true if the request is to be accepted.
 * Should return a reason (human readable description) for why a request has been rejected along
 * with optionally easily parseable information (e.g. range) concerning allowed values.
 */
template<class T>
using ValidationFunction = std::function<std::tuple<std::string, bool>(const std::optional<T> &)>;

/**
 * @brief A ValidationFunction that will just accept any value of type T.
 * @tparam T The type to accept.
 */
template<class T>
const auto
    accept_everything = [](const std::optional<T> & /*v*/) { return std::make_tuple(""s, true); };

/**
 * @brief A ValidationFunction that will accept no request of change. Useful for static settings.
 * @tparam T T The type to reject.
 */
template<class T>
const auto accept_nothing =
    [](const std::optional<T> & /*v*/) {
      return std::make_tuple("static value is read-only"s,
                             false);
    };

/**
 * @brief Generates a ValidationFunction that accepts all values in a range.
 * @tparam T The type of the value to accept.
 * @param low The lowest value to accept.
 * @param high The highest value to accept.
 * @return A ValidationFunction that accepts all values in the given range.
 */
template<class T>
ValidationFunction<T> AcceptRange(T low, T high) {
  return [low = std::move(low),
      high = std::move(high)](const std::optional<T> &v) -> std::tuple<std::string, bool> {
    if (low <= v && v <= high) {
      return {"", true};
    }
    std::stringstream ss;
    ss << "Value must be in range [" << low << ", " << high << "]";
    return {ss.str(), false};
  };
}

/**
 * @brief Generates a ValidationFunction that accepts only the specified values.
 * @tparam T The type of the value to accept.
 * @param values The list of values to accept.
 * @return A ValidationFunction that will only accept the given values.
 */
template<class T>
ValidationFunction<T> AcceptOnly(std::initializer_list<T> values) {
  return [values = std::set<T>(std::move(values))](const std::optional<T> &v)
      -> std::tuple<std::string, bool> {
    if (v.has_value() && values.count(*v) > 0) {
      return {"", true};
    }
    std::stringstream ss;
    ss << "Value must be in {";
    for (auto &v : values) {
      ss << v << ", ";
    }
    ss << "}";
    return {ss.str(), false};
  };
}

template<class T, class U>
inline ValidationFunction<T> AcceptAtLeast(U min) {
  return [min = std::move(min)](const std::optional<T> &v) -> std::tuple<std::string, bool> {
    if (v.has_value() && min <= *v) {
      return {"", true};
    }
    std::stringstream ss;
    ss << "Value must be at least " << min;
    return {ss.str(), false};
  };
}

template<class T>
inline ValidationFunction<T> AcceptAtLeast(T min) {
  return AcceptAtLeast<T, T>(min);
}

template<class T, class U = T>
inline ValidationFunction<T> AcceptAtMost(U max) {
  return [max = std::move(max)](const std::optional<T> &v) -> std::tuple<std::string, bool> {
    if (v.has_value() && *v <= max) {
      return {"", true};
    }
    std::stringstream ss;
    ss << "Value must be at most " << max;
    return {ss.str(), false};
  };
}

template<class T>
inline ValidationFunction<T> AcceptAtMost(T max) {
  return AcceptAtMost<T, T>(max);
}

template<typename X>
constexpr auto has_equal_operator(int /*unused*/) -> decltype(std::declval<X>()
    == std::declval<X>(), bool()) { return true; }
template<typename X>
constexpr bool has_equal_operator(...) { return false; }

/**
 * @brief A distributed setting that can be set via the pulicast framework.
 *
 * Distributed settings are changed by sending requests of changes to the channel associated with
 * the distributed setting. The distributed setting then uses a validation function to decide
 * whether to accept or reject the request of change.
 *
 * A distributed setting can not be changed by the node that owns it to ensure that possible views
 * on the setting stay synchronized. Therefore the DistributedSetting class exposes the interface
 * of ``std::optional`` without all the write functions.
 *
 * @tparam T the type of the distributed setting. Must be sendable via pulicast.
 * Must be default-constructible.
 */
template<class T>
class DistributedSetting {
 public:
  /**
   * @brief Create an uninitialized distributed setting.
   *
   * @param name The name of the distributed setting.
   * @param ns The namespace containing the setting.
   * @param value_changed A callback that is called when the value is changed
   *    (not on each request of change).
   * @param validate_roc A function to validate request of change to the setting.
   * By default everything is accepted.
   */
  DistributedSetting(const std::string &name, Namespace ns,
                     std::function<void()> value_changed = []() {},
                     ValidationFunction<T> validate_roc = accept_everything<T>)
      : value_(std::nullopt), default_value_(std::nullopt),
        value_changed_(std::move(value_changed)), validate_roc_(std::move(validate_roc)),
        confirmation_channel_(ChannelForConfirmations(name, ns)) {
    SetupROCSubscription(name, ns);
  }

  /**
   * @brief Create a new distributed setting with default value.
   *
   * @param initial_value The initial vlaue of the distributed setting.
   * @param name The name of the distributed setting.
   * @param ns The namespace containing the setting.
   * @param value_changed A callback that is called when the value is changed
   *    (not on each request of change).
   * @param validate_roc A function to validate request of change to the setting.
   * By default everything is accepted.
   */
  DistributedSetting(T initial_value, const std::string &name, Namespace ns,
                     std::function<void()> value_changed = []() {},
                     ValidationFunction<T> validate_roc = accept_everything<T>)
      : value_(initial_value), default_value_(initial_value),
        value_changed_(std::move(value_changed)), validate_roc_(std::move(validate_roc)),
        confirmation_channel_(ChannelForConfirmations(name, ns)) {
    SetupROCSubscription(name, ns);
  }

  /**
   * @note This operator does not check whether the setting has been initialized. If checked access
   * is needed, ``value()`` or ``value_or()`` may be used.
   * @return A const reference to the contained value.
   */
  constexpr const T &operator*() const {
    return *value_;
  }

  /**
   * @note This operator does not check whether the setting has been initialized. If checked access
   * is needed, ``value()`` or ``value_or()`` may be used.
   * @return A const reference to the contained std::optional.
   */
  constexpr const std::optional<T> &
  operator->() const {
    // Note: we use the drill-down behavior as described here: https://stackoverflow.com/a/8782794
    return value_;
  }

  /**
   * @brief Checks whether the setting is initialized and contains a value.
   * @return ``true`` if the setting is initialized, ``false`` if not.
   */
  constexpr explicit operator bool() const noexcept {
    return has_value();
  }

  /**
   * @brief Checks whether the setting is initialized and contains a value.
   * @return ``true`` if the setting is initialized, ``false`` if not.
   */
  [[nodiscard]] constexpr bool has_value() const noexcept {
    return value_.has_value();
  }

  /**
   * @brief If the setting is initialized, returns a reference to its value. Otherwise, throws a
   * ``std::bad_optional_access`` exception.
   * @return A reference to the settings value.
   * @throws ``std::bad_optional_access`` if the setting is not initialized.
   * @note ``operator*()`` does not check if the setting is initialized and may be more efficient.
   */
  constexpr const T &value() const &{
    return value_.value();
  }

  /**
   * @brief If the setting is initialized, returns a reference to its value. Otherwise, throws a
   * ``std::bad_optional_access`` exception.
   * @return A reference to the settings value.
   * @throws ``std::bad_optional_access`` if the setting is not initialized.
   * @note ``operator*()`` does not check if the setting is initialized and may be more efficient.
   */
  constexpr const T &&value() const &&{
    return value_.value();
  }

  /**
   * @brief Returns the settings value if it has been initialized, otherwise returns
   * ``default_value``.
   * @tparam U The type of the value to return. See ``std::optional::value_or`` for details.
   * @param default_value The value to use in case the setting is not yet initialized.
   * @return The current value of the setting if it is initialized, or ``default_value`` otherwise.
   */
  template<class U>
  constexpr T value_or(U &&default_value) &&{
    return value_.template value_or(std::forward<U>(default_value));
  }

  /**
   * @brief Returns the settings value if it has been initialized, otherwise returns
   * ``default_value``.
   * @tparam U The type of the value to return. See ``std::optional::value_or`` for details.
   * @param default_value The value to use in case the setting is not yet initialized.
   * @return The current value of the setting if it is initialized, or ``default_value`` otherwise.
   */
  template<class U>
  constexpr T value_or(U &&default_value) const &{
    return value_.template value_or(std::forward<U>(default_value));
  }

 private:
  std::optional<T> value_;
  std::optional<T> default_value_;
  std::function<void()> value_changed_;
  ValidationFunction<T> validate_roc_;
  Channel &confirmation_channel_;

  void SetupROCSubscription(const std::string &name, Namespace &ns) {

    auto &roc_channel = ChannelForRequestsOfChange(name, ns);

    assert(!roc_channel.IsSubscribed() && "The setting already seems to exist "
                                          "since the roc channel is already subscribed!");
    assert(!confirmation_channel_.IsPublished() && "The setting already seems to exist "
                                                   "since its channel is already published!");

    auto handle_roc = [this](const T &roc) {
      auto[warning, accepted] = validate_roc_(roc);
      if (accepted) {
        SetValue(roc);
      }
      SendConfirmation(std::move(warning));
    };

    auto handle_poke = [this](const pulicast_messages::poke_setting & /*unused*/) {
      auto[warning, _] = validate_roc_(std::nullopt);
      SendConfirmation(std::move(warning));
    };

    auto handle_unset = [this](const pulicast_messages::unset_setting & /*unused*/) {
      auto[warning, accepted] = validate_roc_(std::nullopt);
      if (accepted) {
        SetValue(std::nullopt);
      }
      SendConfirmation(std::move(warning));
    };

    auto handle_reset = [this](const pulicast_messages::reset_setting_to_default & /*unused*/) {
      auto[warning, accepted] = validate_roc_(default_value_);
      if (accepted) {
        SetValue(default_value_);
      }
      SendConfirmation(std::move(warning));
    };

    roc_channel.template SubscribeOrdered<T>(handle_roc);
    roc_channel.template SubscribeOrdered<pulicast_messages::poke_setting>(handle_poke);
    roc_channel.template SubscribeOrdered<pulicast_messages::unset_setting>(handle_unset);
    roc_channel.template SubscribeOrdered<pulicast_messages::reset_setting_to_default>(handle_reset);
  }

  void SetValue(const std::optional<T> &new_value) {
    // Note: we only check if the new value is different from the old value if there is an equals
    // operator defined on the value type.
    if constexpr (has_equal_operator<T>(0)) {
      if (new_value.has_value() and new_value == value_) {
        return;
      }
    }
    value_ = new_value;
    value_changed_();
  }

  void SendConfirmation(std::string &&warning) const {
    if (value_) {
      SendConfirmationWithValue(std::move(warning), *value_);
    } else {
      SendConfirmationWithoutValue(std::move(warning));
    }
  }

  void SendConfirmationWithValue(std::string &&warning, const T &value) const {
    auto ack = pulicast_messages::roc_ack();
    ack.message = std::move(warning);

    EncodeAndThenCall(value, [&](const pulicast::core::ScatterGatherBuffer &buffer) {
      auto buffer_it = asio::buffers_begin(buffer);

      ExtractFingerprint(buffer_it, ack.fingerprint);

      auto size_without_fingerprint = asio::buffer_size(buffer) - sizeof(ack.fingerprint);
      ack.value.resize(size_without_fingerprint);
      std::copy(buffer_it, asio::buffers_end(buffer), ack.value.begin());

      ack.len = static_cast<decltype(ack.len)>(ack.value.size());

      confirmation_channel_ << ack;
    });
  }

  void SendConfirmationWithoutValue(std::string &&warning) const {
    auto ack = pulicast_messages::roc_ack();
    ack.message = std::move(warning);

    EncodeAndThenCall(T(), [&](const pulicast::core::ScatterGatherBuffer &buffer) {
      auto buffer_it = asio::buffers_begin(buffer);
      ExtractFingerprint(buffer_it, ack.fingerprint);
      ack.len = 0;
      confirmation_channel_ << ack;
    });
  }

  static void ExtractFingerprint(
      asio::buffers_iterator<pulicast::core::ScatterGatherBuffer> &buffer_it,
      uint8_t(&fingerprint)[8]) {
    for (uint8_t i = 0; i < sizeof(fingerprint); i++, buffer_it++) {
      fingerprint[i] = *buffer_it;
    }
  }
};

}  // namespace pulicast::experimental::distributed_settings

#endif //INCLUDE_PULICAST_DISTRIBUTED_SETTING_H_
