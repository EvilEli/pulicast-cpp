# copied from https://gitlab.com/CLIUtils/modern-cmake/-/blob/master/examples/extended-project/CMakeLists.txt
cmake_minimum_required(VERSION 3.10...3.17)

################################################################################
# Semantic version parsing and setting variables for project and so-versioning #
################################################################################
# get semver string from setuptools_scm
execute_process(
    COMMAND python3 -c "from setuptools_scm import get_version; print('.'.join(get_version(root='${CMAKE_SOURCE_DIR}').split('.')[:3]), end='')"
    OUTPUT_VARIABLE PROJECT_VERSION
    RESULT_VARIABLE VERSION_VALID
  )

#######################################################################
#                           Declare Project                           #
#  checks for cpp linker and compiler and set appropriate variables   #
#######################################################################
project(kiteswarms-pulicast
        DESCRIPTION "UDP publish/subscribe middleware"
        LANGUAGES CXX
        VERSION  ${PROJECT_VERSION}
)

# GNUInstallDirs sets `standard` install directory variables
include(GNUInstallDirs)
# Configure Cmake package variables
include(CMakePackageConfigHelpers)
# Tells Cmake where to put the `kiteswarms-simulator-config.cmake` file
set(INSTALL_CONFIGDIR ${CMAKE_INSTALL_LIBDIR}/cmake/pulicast)

#######################################################################
#                               Options                               #
#######################################################################


option(UNIT_TESTS "Build the tests" OFF)
option(BUILD_SHARED_LIBS "Build shared library" ON)
set(CMAKE_EXPORT_COMPILE_COMMANDS  ON)
option(BUILD_DOCS "Build documenation via --target docs" ON)
option(CREATE_DEB_PACKAGE "option to create a debian package" OFF)

# Only do these if this is the main project, and not if it is included through add_subdirectory
if(CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME)
    # find_package(kiteswarms_cmake_utils REQUIRED)

    ###########################################################################
    #      Testing specifics via Ctest and Googletest                         #
    ###########################################################################
    # Testing only available if this is the main app
    # Note this needs to be done in the main CMakeLists
    # since it calls enable_testing, which must be in the
    # main CMakeLists.
    # Testing only available if this is the main app
    # Emergency override pulicast_BUILD_TESTING provided as well
    include(CTest)
    
    # Docs only available if this is the main app
    if(BUILD_DOCS)
        find_package(Doxygen)
        if(Doxygen_FOUND)
            add_subdirectory(docs)
        else()
            message(STATUS "Doxygen not found, not building docs")
        endif()
    endif()
endif()

#######################################################################
#                               Library                               #
#######################################################################
add_subdirectory(src)

add_subdirectory(examples)
add_subdirectory(apps)

if((CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME OR pulicast_UNIT_TESTS) AND UNIT_TESTS)
    add_subdirectory(tests)
endif()

# This file is required for find_package(pulicast SomeVersion) to be working correctly.
# If library with version SomeVersion is present but there is not version file, it will be ignored.
write_basic_package_version_file(
    ${CMAKE_BINARY_DIR}/cmake/pulicast-config-version.cmake
    VERSION ${pulicast_VERSION}
    COMPATIBILITY SameMajorVersion
)

configure_package_config_file(
    ${CMAKE_SOURCE_DIR}/cmake/pulicast-config.cmake.in
    ${CMAKE_BINARY_DIR}/cmake/pulicast-config.cmake
    INSTALL_DESTINATION ${INSTALL_CONFIGDIR}
)

install(
    FILES
        ${CMAKE_BINARY_DIR}/cmake/pulicast-config.cmake
        ${CMAKE_BINARY_DIR}/cmake/pulicast-config-version.cmake
    DESTINATION ${INSTALL_CONFIGDIR}
)



if((CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME) AND CREATE_DEB_PACKAGE)
    include(cmake/deb_packaging.cmake)
endif()
