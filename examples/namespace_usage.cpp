/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <string>

#include <pulicast/pulicast.h>

int main() {
  pulicast::AsioNode node("rover_controller", "/drivers/rover1");
  auto other_ns = node.MakeNamespace("/flyers/glider1");

  // Access channels in default namespace and other namespace
  std::cout << node["attitude"].GetName() << std::endl;  // drivers/rover1/attitude
  std::cout << other_ns["attitude"].GetName() << std::endl;  // flyers/glider1/attitude

  // Access channels relative to a namespace (default or other)
  std::cout << (node / "arm")["attitude"].GetName() << std::endl;  // drivers/rover1/arm/attitude
  std::cout << (other_ns / "flap")["angle"].GetName() << std::endl;  // flyers/glider1/flap/angle
  std::cout << (other_ns / ".." / "glider2")["attitude"].GetName() << std::endl;  // flyers/glider2/attitude
}

/** @example namespace_usage.cpp
 * This example demonstrates how a node acts as the namespace which as been set as the default one
 * and how namespaces can be used to query channels.
 *
 * @note Namespaces have the "/" operator overloaded to easily access sub-namespaces.
 * When a namespace is acquired via MakeNamespace() from a node, it is resolved from the root
 * namespace. The "/" operator allows to access namespaces relative to the nodes default namespace.
 *
 * This is the assumed channel hierarchy in the above example:
 *
 *
 @verbatim
 ├── drivers
 │   ├── rover1  (the node default namespace)
 │   │   ├── arm
 │   │   │   └── attitude
 │   │   └── attitude
 │   └── rover2
 │       ├── arm
 │       │   └── attitude
 │       └── attitude
 └── flyers  (the other_ns)
     ├── glider1
     │   ├── attitude
     │   └── flap
     │       └── angle
     └── glider2
         ├── attitude
         └── flap
             └── angle
 @endverbatim
 */
