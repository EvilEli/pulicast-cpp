/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <forward_list>

#include <pulicast/pulicast.h>
#include <pulicast/transport/asio_transport.h>

int main() {
  auto node = pulicast::AsioNode("Test");
  auto messages = std::forward_list<std::string>();

  auto notification = []() {
    std::cout << "Got Message!" << std::endl;
  };

  auto message_handler = [&](const std::string &msg) {
    messages.push_front(msg);
  };

  auto message_handler_info =
      [&](const std::string &msg, const pulicast::MessageInfo &msg_info) {
        messages.push_front(msg);
      };

  auto message_handler_opt = [&](const std::optional<std::string> &msg) {
    if (msg.has_value())
      messages.push_front(*msg);
  };

  auto message_handler_opt_info =
      [&](const std::optional<std::string> &msg, const pulicast::MessageInfo &msg_info) {
        if (msg.has_value())
          messages.push_front(*msg);
      };

  auto strand1 = asio::io_service::strand(*node.GetService());
  auto wrapped_notification = pulicast::transport::wrap(notification, strand1);
  auto wrapped_handler = pulicast::transport::wrap<std::string>(message_handler, strand1);
  auto wrapped_handler_opt = pulicast::transport::wrap<std::string>(message_handler_opt, strand1);
  auto wrapped_handler_info = pulicast::transport::wrap<std::string>(message_handler_info, strand1);
  auto wrapped_handler_opt_info = pulicast::transport::wrap<std::string>(message_handler_opt_info, strand1);

  node["strand1"].Subscribe(wrapped_notification);
  node["strand1"].Subscribe<std::string>(wrapped_handler);
  node["strand1"].Subscribe<std::string>(wrapped_handler_opt);
  node["strand1"].Subscribe<std::string>(wrapped_handler_info);
  node["strand1"].Subscribe<std::string>(wrapped_handler_opt_info);

  node.GetService()->run();
}

/** @example subscribe_with_strands.cpp
 * This example demonstrates how to wrap various subscription handlers to an asio::strand.
 * Note that the strand here has no practical purpose since the example is single-threaded.
 * See `multi_threading_with_strands.cpp` for a more advanced example.
 */