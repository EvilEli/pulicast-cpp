/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <thread>
#include <forward_list>

#include <asio.hpp>

#include <pulicast/pulicast.h>
#include <pulicast/transport/asio_transport.h>

class LogMessageCollector {
 public:
  LogMessageCollector(asio::io_service &service,
                      std::initializer_list<std::reference_wrapper<pulicast::Channel> > channels)
      : strand_(service) {
    auto collect_logmessage =
        pulicast::transport::wrap<std::string>([this](const std::string &msg) {
          collected_messages_.push_back(msg);
        }, strand_);

    // Note: try those lines instead of the ones above to try what happens with out synchronization
    //   by asio::strand.
//    auto collect_logmessage =[this](const std::string &msg) {
//          collected_messages_.push_back(msg);
//          std::cout << msg.msg;
//        };

    for (auto &channel : channels) {
      channel.get().Subscribe<std::string>(collect_logmessage);
    }
  }
 private:
  asio::io_service::strand strand_;
  std::vector<std::string> collected_messages_;
};

class LogMessageProducer {
 public:
  LogMessageProducer(asio::io_service &service,
                     std::initializer_list<std::reference_wrapper<pulicast::Channel> > channels)
      : channels_(channels), service_(service) {
    ProduceLogs();
  }
 private:
  std::initializer_list<std::reference_wrapper<pulicast::Channel> > channels_;
  asio::io_service &service_;

  void ProduceLogs() {
    for (auto &channel : channels_) {
      channel.get() << "Hi There!";
    }
    service_.post([this]() { ProduceLogs(); });
  }

};

int main() {
  auto node = pulicast::AsioNode("Test");
  auto messages = std::forward_list<std::string>();

  auto col1 = LogMessageCollector(*node.GetService(),
                                  {node["ch1"], node["ch2"], node["ch3"]});
  auto col1_1 = LogMessageCollector(*node.GetService(),
                                    {node["ch1"], node["ch2"], node["ch3"]});
  auto col2 = LogMessageCollector(*node.GetService(),
                                  {node["ch4"], node["ch5"], node["ch6"]});
  auto prod = LogMessageProducer(*node.GetService(),
                                 {node["ch1"], node["ch2"], node["ch3"],
                                  node["ch4"], node["ch5"], node["ch6"]});

  auto t1 = std::thread([&]() {node.GetService()->run();});
  auto t2 = std::thread([&]() {node.GetService()->run();});
  auto t3 = std::thread([&]() {node.GetService()->run();});
  auto t4 = std::thread([&]() {node.GetService()->run();});

  t1.join();
  t2.join();
  t3.join();
  t4.join();
}

/** @example multi_threading_with_strands.cpp
 * This example demonstrates how to develop a multi-threaded application using`asio::strand` object.
 *
 * The two `LogMessageCollector`s subscribe on multiple channels to `std::string`s and store
 * them in a `std::vector` each.
 * The `LogMessageProducer` writes log messages to a number of channels.
 *
 * When the application is single-threaded there is no concurrency problem. As soon as a second
 * thread is introduced however, concurrent access to the `collected_messages_` member of the
 * `LogMessageCollector` can cause trouble. Therefore the `LogMessageCollector` wraps its
 * subscription handlers in a strand (line 22-25) to avoid their concurrent execution.
 * This way both threads can be fully used by both message collectors (and the log message producer)
 * without race conditions on `collected_messages_`.
 *
 * Try switching line 22-25 with line 27-32 to see what happens when the subscription handlers are
 * not wrapped in a strand (hint: segfault).
 */
