/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <string>

#include <pulicast/pulicast.h>

int main() {
  pulicast::AsioNode node("simple_subscriber");

  node["example_channel"].Subscribe<std::string>(
      [](const std::string &msg) {
        std::cout << msg << std::endl;
      });

  node.GetService()->run();
}

/** @example subscribe.cpp
 * This code subscribes to `std::string` messages on the `example_channel` and prints them to
 * stdout.
 *
 * @note The `run` function of the asio io service blocks and therefore prevents the program from
 * exiting immediately. It is responsible for spawning subscription callbacks so if you do not call
 * it your subscriptions will not work!
 */