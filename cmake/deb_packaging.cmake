include(InstallRequiredSystemLibraries)

# get semver string from setuptools_scm
execute_process(
        COMMAND python3 -c "from setuptools_scm import get_version\nprint(get_version(root='${CMAKE_CURRENT_SOURCE_DIR}'), end='')"
        OUTPUT_VARIABLE CPACK_PACKAGE_VERSION
        RESULT_VARIABLE VERSION_VALID
)

if(NOT "${VERSION_VALID}" STREQUAL "0")
    message( FATAL_ERROR "Could not fetch SemVer string" )
else()
    message( "Semantic Version: ${CPACK_PACKAGE_VERSION}" )
endif()


set(CPACK_PACKAGE_NAME "pulicast")
set(CPACK_PACKAGE_VENDOR "Kiteswarms Ltd")
set(CPACK_SET_DESTDIR "on" )
set(CPACK_INSTALL_PREFIX "/usr" )
set(CPACK_PACKAGE_CONTACT "Maximilian Ernestus <maximilian@ernestus.de>" )
set(CPACK_PACKAGE_HOMEPAGE_URL "https://gitlab.com/kiteswarms/pulicast-cpp" )
set(CPACK_PACKAGE_FILE_NAME "${CPACK_PACKAGE_NAME}_${CPACK_PACKAGE_VERSION}" )
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "UDP publish/subscribe middleware")
set(CPACK_SOURCE_PACKAGE_FILE_NAME "${CPACK_PACKAGE_NAME}_${CPACK_PACKAGE_VERSION}" )
set(CPACK_COMPONENTS_ALL Libraries ApplicationData )

set(CPACK_GENERATOR "DEB")
set(CPACK_DEBIAN_PACKAGE_DEPENDS "libasio-dev (>= 1.10.8), zcm (>= 2.0.0), libboost-program-options-dev (>= 1.65.1)")
set(CPACK_DEBIAN_PACKAGE_GENERATE_SHLIBS ON)
set(CPACK_DEBIAN_PACKAGE_BREAKS ${CPACK_PACKAGE_NAME} )
include(CPack)
