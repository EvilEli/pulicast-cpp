/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <asio/buffer.hpp>

#include <pulicast/pulicast.h>
#include <pulicast/distributed_setting.h>

#include "utils/mock_node.h"

using namespace pulicast::experimental;

// TODO(max): this test fails when the puliscope is active! Why?
TEST(TestDistributedSetting, roc_should_be_answered) {  // NOLINT
  // GIVEN
  auto setting_name = "my_setting";
  auto node = pulicast::AsioNode("mono"); // TODO(max): better use a mock node here!

  auto& roc_channel = distributed_settings::ChannelForRequestsOfChange(setting_name, node);
  auto &confirmation_channel = distributed_settings::ChannelForConfirmations(setting_name, node);

  auto mock_confirmation_subscription = ::testing::MockFunction<void(const pulicast_messages::roc_ack&)>();
  confirmation_channel.Subscribe<pulicast_messages::roc_ack>(mock_confirmation_subscription.AsStdFunction());

  auto setting = distributed_settings::DistributedSetting<int>(5, setting_name, node, [](){}, [](const std::optional<int> &new_value) {
    return std::make_tuple(std::string("Its always gonna be 5!"), false);
  });

  // THEN
  EXPECT_CALL(mock_confirmation_subscription, Call).Times(1);

  // WHEN
  roc_channel << int{1};

  node.GetService()->run_for(1s);
}

TEST(TestDistributedSetting, roc_should_change_value_and_notify_cb) {  // NOLINT
  // GIVEN
  auto setting_name = "my_setting";
  auto node = pulicast::AsioNode("mono"); // TODO(max): better use a mock node here!
  auto& roc_channel = distributed_settings::ChannelForRequestsOfChange(setting_name, node);
  auto on_value_changed = ::testing::MockFunction<void(void)>();

  auto setting = distributed_settings::DistributedSetting<int>(5, setting_name, node, on_value_changed.AsStdFunction());

  // THEN
  EXPECT_CALL(on_value_changed, Call()).Times(1);

  // WHEN
  roc_channel << int{1};
  node.GetService()->poll();

  // THEN
  EXPECT_EQ(*setting, 1);
}

TEST(TestDistributedSetting, dummy_invoke_test) {  // NOLINT
  auto node = pulicast::testutils::MockNode();

  auto setting1 = distributed_settings::DistributedSetting<int>("test_value3", node);
  auto setting2 = distributed_settings::DistributedSetting<int>(42, "test_value4", node);

  EXPECT_FALSE(setting1.has_value());
  EXPECT_FALSE(setting1);

  EXPECT_TRUE(setting2.has_value());
  EXPECT_TRUE(setting2);

  EXPECT_EQ(*setting2, 42);
  EXPECT_EQ(setting2.value(), 42);
}