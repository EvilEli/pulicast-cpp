/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <string>

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <boost/algorithm/string.hpp>
#include <pulicast/core/utils/name_resolution.h>


class NameResolution : public testing::TestWithParam<std::tuple<std::string, std::string> > {};

INSTANTIATE_TEST_SUITE_P(ValidInputs, NameResolution, ::testing::Combine(
    ::testing::Values("", "a", "asd", "asd1/asd2", "asd1/asd2/"),  // name
    ::testing::Values("", "/ns", "ns/", "ns1/ns2")));  // namespace

TEST_P(NameResolution, TestThatSlashPrefixCausesGlobalLookup) { //NOLINT
  // GIVEN
  auto [name, name_space] = GetParam();
  auto trimmed_name = boost::trim_copy_if(name, boost::is_any_of("/"));

  // WHEN
  auto resolved_name = pulicast::core::ResolveName("/" + name, name_space);

  // THEN
  EXPECT_EQ(resolved_name, trimmed_name);
}

TEST_P(NameResolution, TestThatNamespaceBecomesPrefix) { //NOLINT
  // GIVEN
  auto [name, name_space] = GetParam();

  // WHEN
  auto resolved_name = pulicast::core::ResolveName(name, name_space);
  auto resolved_namespace = pulicast::core::ResolveName(name_space, "/");

  // THEN
  EXPECT_THAT(resolved_name, testing::StartsWith(resolved_namespace));
}


TEST_P(NameResolution, TestThatNameBecomesPostfix) { //NOLINT
  // GIVEN
  auto [name, name_space] = GetParam();
  auto trimmed_name = boost::trim_copy_if(name, boost::is_any_of("/"));

  // WHEN
  auto resolved_name = pulicast::core::ResolveName("/" + name, "ns/");

  // THEN
  EXPECT_THAT(resolved_name, testing::EndsWith(trimmed_name));
}

TEST(RelativeResolution, TestThatRelativeNamesAreResolved) {  // NOLINT
  // Switch to parent ns
  EXPECT_EQ(pulicast::core::ResolveName("../channel", "l1/"), "channel");
  EXPECT_EQ(pulicast::core::ResolveName("../channel", "l1/"), "channel");
  EXPECT_EQ(pulicast::core::ResolveName("../channel", "l1/l2"), "l1/channel");
  EXPECT_EQ(pulicast::core::ResolveName("../../channel", "l1/l2"), "channel");

  // Switch to sibling ns
  EXPECT_EQ(pulicast::core::ResolveName("../l3/channel", "l1/l2"), "l1/l3/channel");

  // Switching beyond root falls back to root
  EXPECT_EQ(pulicast::core::ResolveName("../channel", "/"), "channel");
  EXPECT_EQ(pulicast::core::ResolveName("../../channel", "/l1"), "channel");
  EXPECT_EQ(pulicast::core::ResolveName("../l2/../../channel", "/l1"), "channel");
}

TEST(RelativeResolution, TestThatRelativeNamespacesAreResolved) {  // NOLINT
  // Switch to parent ns
  EXPECT_EQ(pulicast::core::ResolveName("channel", "l1/.."), "channel");
  EXPECT_EQ(pulicast::core::ResolveName("channel", "l1/l2/.."), "l1/channel");
  EXPECT_EQ(pulicast::core::ResolveName("channel", "l1/l2/../.."), "channel");

  // Switch to sibling ns
  EXPECT_EQ(pulicast::core::ResolveName("channel", "l1/../l2"), "l2/channel");

  // Switching beyond root falls back to root
  EXPECT_EQ(pulicast::core::ResolveName("channel", "l1/../.."), "channel");
}

TEST(RelativeResolution, TestThatRelativeDoubleSlashesAreIgnored) {  // NOLINT
  EXPECT_EQ(pulicast::core::ResolveName("..//channel", "l1/"), "channel");
  EXPECT_EQ(pulicast::core::ResolveName("//channel", "l1/"), "channel");
  EXPECT_EQ(pulicast::core::ResolveName("../..//channel", "l1/l2"), "channel");
}