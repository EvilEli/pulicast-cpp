/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <gtest/gtest.h>
#include "utils/mock_node.h"

/**
 * @note These tests assume the following channel structure
 *
 *
 * ├── drivers
 * │   ├── rover1  (the node default namespace)
 * │   │   ├── arm
 * │   │   │   └── attitude
 * │   │   └── attitude
 * │   └── rover2
 * │       ├── arm
 * │       │   └── attitude
 * │       └── attitude
 * └── flyers  (the other_ns)
 *     ├── glider1
 *     │   ├── attitude
 *     │   └── flap
 *     │       └── angle
 *     └── glider2
 *         ├── attitude
 *         └── flap
 *             └── angle
 */

TEST(NameSpace, NamespaceShouldBePrependedToChannelName) {  //NOLINT
  // GIVEN
  pulicast::testutils::MockNode node("rover_controller", "/drivers/rover1");
  auto other_ns = node.MakeNamespace("/flyers/glider1");

  // THEN
  EXPECT_EQ(node["attitude"].GetName(), "drivers/rover1/attitude");
  EXPECT_EQ(other_ns["attitude"].GetName(), "flyers/glider1/attitude");
}

TEST(NameSpace, RelativeAccessShouldBeResolved) {  //NOLINT
  // GIVEN
  pulicast::testutils::MockNode node("rover_controller", "/drivers/rover1");
  auto other_ns = node.MakeNamespace("/flyers/glider1");

  // THEN
  EXPECT_EQ((node / "arm")["attitude"].GetName(), "drivers/rover1/arm/attitude");
  EXPECT_EQ((other_ns / "flap")["angle"].GetName(), "flyers/glider1/flap/angle");
  EXPECT_EQ((other_ns / ".." / "glider2")["attitude"].GetName(), "flyers/glider2/attitude");
}