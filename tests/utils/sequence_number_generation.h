/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef TESTS_UTILS_SEQUENCE_NUMBER_GENERATION_H_
#define TESTS_UTILS_SEQUENCE_NUMBER_GENERATION_H_

#include <random>
namespace pulicast::testutils {

using SequenceNumberSeries = std::unordered_map<pulicast::Address,
std::vector<uint32_t>,
pulicast::AddressHash,
pulicast::AddressEqual>;
/**
 * Will chose a random first sequence number if not specified.
 *
 * @param length The length of the sequence.
 * @param first_seq_no The sequence number to start with.
 */
inline auto MakeSeqNumberSeries(size_t length, std::optional<uint32_t> first_seq_no = std::nullopt) {
  auto series = std::vector<uint32_t>(length);
  std::iota(series.begin(), series.end(), first_seq_no.value_or(std::rand() % 1000));
  return series;
}

/**
 * Creates sequence number series for multiple sources and returns them in a dict.
 *
 * @param num_sources The number of sources to generate sequence number series for.
 * @param series_length The length of the sequence number series.
  */
inline auto MakeSequenceNumberSeriesForMultipleSources(size_t num_sources, size_t series_length) {
  auto seq_no_series = SequenceNumberSeries();
  for (uint32_t i = 0; i < num_sources; i++) {
    seq_no_series[pulicast::Address{i, 0}] = MakeSeqNumberSeries(series_length);
  }
  return seq_no_series;
}

/**
 * Removes one 'package sent' event from the sequence number series and returns it.
 * Basically this first chooses any of the sources and pops the next sequence number from its list.
 * Will also remove a source from the dictionary of sequence number series when the series becomes
 * empty.
 *
 * @param seq_no_series_by_source A dictionary mapping sources to lists of sequence numbers.
 * @return A tuple of the Address and the sequence number that was chosen by the function.
 */
inline std::tuple<pulicast::Address, uint32_t>
TakeOneSourceAndSeqNo(SequenceNumberSeries &seq_no_series_by_source) {
  auto choice = seq_no_series_by_source.begin();
  std::advance(choice, std::rand() % seq_no_series_by_source.size());

  auto source = choice->first;  // copy this to avoid memory leak
  auto &seq_no_series = choice->second;  // reference this so the original list gets modified

  auto seq_no = seq_no_series.front();
  seq_no_series.erase(seq_no_series.begin());

  if (seq_no_series.empty()) {
    seq_no_series_by_source.erase(choice);
  }
  return {source, seq_no};
}

/**
 * Will take one sequence number after the other from the dictionary of sequence number series
 * (with the sources taking turns randomly) and computes the lead of them using the given
 * dispatcher.
 *
 * @param seq_no_series_by_source The series of sequence numbers for each source.
 * @param dispatcher The dispatcher to use for lead calculation.
 * @return The resulting list of message leads.
 */
inline auto ConsumeSeqNoSequences(SequenceNumberSeries &seq_no_series_by_source,
                           pulicast::core::LeadExtractor& lead_extractor) {
  auto leads = std::vector<int>();

  while (seq_no_series_by_source.size() > 0) {
    auto[source, seq_no] = TakeOneSourceAndSeqNo(seq_no_series_by_source);
    auto network_seq_no = htonl(seq_no);
    auto buffer = asio::mutable_buffer(&network_seq_no, sizeof(network_seq_no));
    leads.push_back(*lead_extractor.ExtractLead(buffer, source));
  }
  return leads;
}

inline auto MakeSeqNumberSeriesWithOneDelayedMessage(const int delayed_message, const int delay,
                                              const std::optional<int> &first_seq_no = std::nullopt) {
  auto seq_no_series = MakeSeqNumberSeries((delayed_message + delay) * 2, first_seq_no);
  auto i = seq_no_series[delayed_message];
  seq_no_series.erase(seq_no_series.begin() + delayed_message);
  seq_no_series.insert(seq_no_series.begin() + delayed_message + delay, i);
  return seq_no_series;
}

inline std::vector<uint32_t> MakeSeqNumberSeriesWithBurstOfLostMessages(const int message_loss_start,
                                                                 const int num_lost_messages,
                                                                 std::optional<int> first_seq_no = std::nullopt) {
  first_seq_no = first_seq_no.value_or(std::rand() % 1000);
  auto burst_start = *first_seq_no + message_loss_start;
  auto burst_end = burst_start + num_lost_messages;
  auto last_seq_no = burst_end * 2;
  auto seq_no_series = std::vector<uint32_t>(last_seq_no - *first_seq_no - num_lost_messages);
  std::iota(seq_no_series.begin(), seq_no_series.begin() + message_loss_start, *first_seq_no);
  std::iota(seq_no_series.begin() + message_loss_start, seq_no_series.end(), burst_end);
  return seq_no_series;
}

inline auto make_randomly_ordered_seq_nos_with_some_missing(size_t num_seq_nos,
                                                     int num_missing_packets,
                                                     std::optional<uint32_t> first_seq_no = std::nullopt) {
  auto seq_nos = MakeSeqNumberSeries(num_seq_nos + num_missing_packets, first_seq_no);

  std::random_device rd;
  std::mt19937 g(rd());
  std::shuffle(seq_nos.begin(), seq_nos.end(), g);

  if (num_missing_packets > 0) {
    for (int i = 0; i < num_missing_packets; i++) {
      seq_nos.erase(seq_nos.begin() + (std::rand() % seq_nos.size()));
    }
  }
  return seq_nos;
}

bool is_ordered_seq_no_sequence(const std::map<int, int>& seq_nos_by_pos) {
  for(auto it = seq_nos_by_pos.begin(); it != std::prev(seq_nos_by_pos.end()); it++) {
    const auto& [pos, seq_no] = *it;
    const auto& [next_pos, next_seq_no] = *std::next(it);
    assert(pos < next_pos);
    if (seq_no > next_seq_no) return false;
    if (next_pos - pos != next_seq_no - seq_no) return false;
  }
  return true;
}

}  // namespace pulicast::testutils

#endif //TESTS_UTILS_SEQUENCE_NUMBER_GENERATION_H_
