/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef TESTS_UTILS_MOCK_TRANSPORTS_H_
#define TESTS_UTILS_MOCK_TRANSPORTS_H_

#include <array>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <asio/buffer.hpp>
#include <asio/buffers_iterator.hpp>
#include <vector>

namespace pulicast::testutils {

/**
 * @brief Save the Arguments of the Called Send function to a std::vector<char>
 *
 * Example Usage:
 *     std::vector<char> send_buffer;
 *     EXPECT_CALL(transport_Mock, Send).WillOnce(SaveSendArgs<0>(&send_buffer));
 */
ACTION_TEMPLATE(SaveSendArgs, HAS_1_TEMPLATE_PARAMS(int, k), AND_1_VALUE_PARAMS(pointer)) {
  auto buffers = static_cast<const asio::const_buffer *>(::std::get<0>(args));
  size_t nr_buffers = ::std::get<1>(args);
  auto buffer_sequence = std::vector<asio::const_buffer>(buffers, buffers + nr_buffers);
  auto packet_bytes =
      std::vector<char>(asio::buffers_begin(buffer_sequence), asio::buffers_end(buffer_sequence));
  *pointer = packet_bytes;
}

class MockChanelSender : public pulicast::core::ChannelSender {
 public:
  MOCK_METHOD(void, Send, (pulicast::core::ScatterGatherBuffer & ));
};

class MockTransport : public pulicast::core::Transport {
 public:
  std::unique_ptr<pulicast::core::ChannelSender> MakeChannelSender(const std::string &channel) override {
    auto sender = std::make_unique<MockChanelSender>();
    senders_by_channel_name_[channel] = sender.get();
    return sender;
  }

  void StartReceivingMessagesFor(const std::string &channel,
                                 std::function<void(asio::mutable_buffer &, Address)> cb) override {
    subscriptions_[channel] = cb;
  }

  MockChanelSender &GetChannelSender(const std::string &channel_name) {
    return *senders_by_channel_name_[channel_name];
  }


  /**
  * @brief Receive a pulicast packet from a specific source.
  * This function can be used for testing if a new packet arrives from outside.
  *
  * @param packet the Pulicast Packet which should be received
  * @param source the source of the packet
  * @return true if the callable has a value
  * @return false if there is no callable for the transport
  */
  bool Receive(asio::mutable_buffer &data, Address source, const std::string &channel) {
    if (subscriptions_.count(channel) > 0) {
      subscriptions_[channel](data, source);
      return true;
    } else {
      return false;
    }
  }

  std::unordered_map<std::string, std::function<void(asio::mutable_buffer &, Address)>> subscriptions_;
  std::unordered_map<std::string, MockChanelSender *> senders_by_channel_name_;
};

}  // namespace pulicast::testutils

#endif  // TESTS_UTILS_MOCK_TRANSPORTS_H_
