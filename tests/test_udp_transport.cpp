/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <random>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <asio/buffers_iterator.hpp>

#include <pulicast/transport/udp_transport.h>
#include <pulicast/core/utils/scatter_gather_buffer.h>

using namespace std::string_literals;

class MockUdpChannelSender : public pulicast::transport::UDPChannelSender<MockUdpChannelSender> {
 public:
  MockUdpChannelSender(const std::string &channel_name)
      : UDPChannelSender<MockUdpChannelSender>(channel_name) {}
  MOCK_METHOD(void, SendToSocket, (pulicast::core::ScatterGatherBuffer & ));
};

// TODO(max): parameterize this test to catch the case where
//  * the channel name is empty/too long
//  * the payload is empty/too long
TEST(TestUDPSender, udp_header_should_be_prepended_to_payload) {  // NOLINT
  // GIVEN
  auto channel_name = "channel_name"s;
  auto payload = "payload"s;
  auto cs = MockUdpChannelSender(channel_name);
  auto sent_data = std::string();
  ON_CALL(cs, SendToSocket).WillByDefault([&](pulicast::core::ScatterGatherBuffer &data) {
    sent_data = {asio::buffers_begin(data), asio::buffers_end(data)};
  });

  // THEN
  EXPECT_CALL(cs, SendToSocket).Times(1);

  // WHEN
  auto buffer = pulicast::core::ScatterGatherBuffer{asio::buffer(payload)};
  cs.Send(buffer);

  // THEN
  // size should match
  EXPECT_THAT(sent_data,
              testing::SizeIs(pulicast::transport::kPacketHeader.size() + 1 + channel_name.size()
                                  + payload.size()));
  EXPECT_THAT(sent_data, testing::StartsWith("PULI"));
  EXPECT_THAT(sent_data, testing::EndsWith(payload));
  EXPECT_THAT(sent_data, testing::HasSubstr(channel_name));
  EXPECT_EQ(sent_data[4], channel_name.size());
}

TEST(TestExtractHeaderAndChannelName, packed_header_should_be_extracted_as_is) {  // NOLINT
  // GIVEN
  auto channel_name = "channel_name"s;
  auto payload = "payload"s;
  auto cs = MockUdpChannelSender(channel_name);
  auto sent_data = std::string();
  ON_CALL(cs, SendToSocket).WillByDefault([&](pulicast::core::ScatterGatherBuffer &data) {
    sent_data = {asio::buffers_begin(data), asio::buffers_end(data)};
  });

  // THEN
  EXPECT_CALL(cs, SendToSocket).Times(1);

  // WHEN
  auto in_buffer = pulicast::core::ScatterGatherBuffer{asio::buffer(payload)};
  cs.Send(in_buffer);

  auto out_buffer = asio::buffer(sent_data);
  auto extracted_channel_name = pulicast::transport::ExtractHeaderAndChannelName(out_buffer);
  auto extracted_payload = std::string(
      asio::buffer_cast<char *>(out_buffer), asio::buffer_size(out_buffer));

  // THEN
  EXPECT_THAT(extracted_channel_name, testing::Optional(testing::StrEq(channel_name)));
  EXPECT_THAT(extracted_payload, testing::StrEq(payload));
}

TEST(TestExtractHeaderAndChannelName, random_data_should_fail_to_unpack) {  // NOLINT
  // GIVEN
  auto length = size_t{100};
  auto engine = std::default_random_engine(std::random_device{}());
  auto distribution = std::uniform_int_distribution<char>{};

  for(int i = 0; i < 100000; i++) {
    auto received_bytes = std::vector<char>(length);
    std::generate(received_bytes.begin(),
                  received_bytes.end(),
                  [&]() { return distribution(engine); });

    // WHEN
    auto buffer = asio::buffer(received_bytes);
    auto channel_name = pulicast::transport::ExtractHeaderAndChannelName(buffer);

    // THEN
    EXPECT_EQ(channel_name, std::nullopt);
  }
}