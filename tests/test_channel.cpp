/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <asio/buffers_iterator.hpp>
#include <pulicast/channel.h>

#include "utils/mock_transports.h"

using CH = pulicast::Channel;

MATCHER_P(EqAsioBufferSequence, other, "Equality matcher for asio buffer") { //NOLINT
  auto arg_size = asio::buffers_end(arg) - asio::buffers_begin(arg);
  auto other_size = asio::buffers_end(other) - asio::buffers_begin(other);
  if (arg_size != other_size) {
    *result_listener << "BufferSequences have different length: " << arg_size << " and " << other_size;
    return false;
  }
  auto [diff_other, diff_arg] = std::mismatch(asio::buffers_begin(other), asio::buffers_end(other),
                                        asio::buffers_begin(arg), asio::buffers_end(arg));
  if (diff_other != asio::buffers_end(other)) {
    *result_listener << "BufferSequences differ at: " << diff_other - asio::buffers_begin(other) << ": " << +*diff_other << " and " << +*diff_arg;
    return false;
  }
  if (diff_arg != asio::buffers_end(arg)) {
    *result_listener << "BufferSequences differ at: " << diff_arg - asio::buffers_begin(other) << ": " << +*diff_other << " and " << +*diff_arg;
    return false;
  }
  return true;
}

using MessageInputs = std::tuple<std::string, std::vector<char> >;

class PublishValidMessages
    : public testing::TestWithParam<MessageInputs> {};

INSTANTIATE_TEST_SUITE_P( //NOLINT
    ValidMessages, PublishValidMessages,
    ::testing::Values(MessageInputs("Channel", {'a', 'b', 'c', 'd'}),
                      MessageInputs(std::string("\0ch\0a\n", 6), {'a', '\0', '\r', '\t'}),
                      MessageInputs(std::string(128, 'A'), {'\\', '\0', '\r', '\t'}),
                      MessageInputs(std::string(255, 'B'), {'\0', '#', '$', '`', '#', ',', '>'}),
                      MessageInputs("", {'\0', '#', '$', '`', '#', ',', '>'}),
                      MessageInputs("", {})));

TEST_P(PublishValidMessages, HandlePositiveInputs) { //NOLINT
  // GIVEN
  auto [channel_name, message_bytes] = GetParam();

  auto mock_transport = pulicast::testutils::MockTransport();

  auto channel = CH(channel_name, mock_transport);
  EXPECT_FALSE(channel.IsPublished());
  EXPECT_FALSE(channel.IsSubscribed());
  EXPECT_TRUE(channel.GetName() == channel_name);

  // Then
  // generate true values
//  uint32_t sequence_number = 0;
//  auto packet = pulicast::core::PulicastPacket::FromFields(
//      sequence_number, channel_name, message_bytes.data(), message_bytes.size());
//  auto scattered_packet_bytes = packet->Pack();

  // Check call
  EXPECT_CALL(mock_transport.GetChannelSender(channel_name), Send).Times(1);  //TODO(max): check for arguments here as well NOLINT

  // WHEN
  channel.Publish(message_bytes.data(), message_bytes.size());

  // THEN
  EXPECT_TRUE(channel.IsPublished());//NOLINT
}

