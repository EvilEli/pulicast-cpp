/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <numeric>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <pulicast/core/lead_extractor.h>

#include "utils/sequence_number_generation.h"

using namespace pulicast::testutils;

class NoDelaysOrLosses : public testing::TestWithParam<size_t> {
 protected:
  void SetUp() override {
    seq_no_series = MakeSequenceNumberSeriesForMultipleSources(GetParam(), 100);
  }
  size_t NumberOfMessages() { return 100 * GetParam(); }
  SequenceNumberSeries seq_no_series;
};

INSTANTIATE_TEST_SUITE_P(SingleMessageSource, NoDelaysOrLosses,
                         ::testing::Values(1)  // number of message sources
);
INSTANTIATE_TEST_SUITE_P(MultipleMessageSources, NoDelaysOrLosses,
                         ::testing::Values(2, 5, 10)  // number of message sources
);

/**
 * Test if lead computation works correctly if there is no message loss and no out of order messages.
 */
TEST_P(NoDelaysOrLosses, lead_is_one) {  // NOLINT
  // GIVEN
  auto extractor = pulicast::core::LeadExtractor();

  // WHEN
  auto leads = ConsumeSeqNoSequences(seq_no_series, extractor);

  // THEN
  EXPECT_THAT(leads, ::testing::Each(1));
  EXPECT_THAT(leads, ::testing::SizeIs(NumberOfMessages()));
}

TEST(LeadExtractorTest, unparseable_data_yields_nullopt) {  // NOLINT
  // GIVEN
  auto extractor = pulicast::core::LeadExtractor();
  auto network_seq_no = char('c');
  auto buffer = asio::mutable_buffer(&network_seq_no, sizeof(network_seq_no));

  // WHEN
  auto extracted_lead = extractor.ExtractLead(buffer, {0, 0});

  // THEN
  EXPECT_EQ(extracted_lead, std::nullopt);
}

class SingleDelayedMessage : public testing::TestWithParam<std::tuple<int, int> > {  // NOLINT
 protected:
  void SetUp() override {
    seq_no_series[{0, 0}] = MakeSeqNumberSeriesWithOneDelayedMessage(
        IdxOfMessageAfterDelayedMessage(),
        MessageDelay(),
        10);
  }
  int IdxOfMessageAfterDelayedMessage() { return std::get<0>(GetParam()); }
  int IdxOfDelayedMessage() { return IdxOfMessageAfterDelayedMessage() + MessageDelay(); }
  int MessageDelay() { return std::get<1>(GetParam()); }
  bool FirstMessageIsDelayed() { return IdxOfMessageAfterDelayedMessage() == 0; }
  SequenceNumberSeries seq_no_series;
};

INSTANTIATE_TEST_SUITE_P(LaterMessageDelayed, SingleDelayedMessage, ::testing::Combine(
    ::testing::Values(1, 5, 30, 50),  // idx of message to delay
    ::testing::Values(1, 3, 5, 10)));  // delay

INSTANTIATE_TEST_SUITE_P(FirstMessageDelayed, SingleDelayedMessage, ::testing::Combine(
    ::testing::Values(0),  // idx of message to delay
    ::testing::Values(1, 3, 5, 10)));  // delay

TEST_P(SingleDelayedMessage, lead_should_be_negative_delay) {  // NOLINT
  // GIVEN
  auto extractor = pulicast::core::LeadExtractor();

  // WHEN
  auto leads = ConsumeSeqNoSequences(seq_no_series, extractor);

  // THEN
  EXPECT_EQ(leads[IdxOfDelayedMessage()], -MessageDelay())
            << "The delayed messages lead should indicate by how much it was delayed.";
}

TEST_P(SingleDelayedMessage, first_message_after_delayed_message_should_have_lead_2) {  // NOLINT
  // GIVEN
  auto extractor = pulicast::core::LeadExtractor();

  // WHEN
  auto leads = ConsumeSeqNoSequences(seq_no_series, extractor);

  // THEN
  // Note: When the first message is delayed then there is not way to figure that out
  if (!FirstMessageIsDelayed()) {
    EXPECT_EQ(leads[IdxOfMessageAfterDelayedMessage()], 2)
              << "The first message after the delayed message arrived should have a lead of 2 because it is early.";
  }
}

class RandomSequenceNumbers : public testing::TestWithParam<int> {
 protected:
  void SetUp() override {
    sequence_numbers = std::vector<uint32_t>(200);
    auto engine = std::default_random_engine(GetParam());
    auto distribution = std::uniform_int_distribution<uint32_t>(0, 100);
    std::generate(sequence_numbers.begin(),
                  sequence_numbers.end(),
                  [&]() { return distribution(engine); });
  }
  std::vector<uint32_t> sequence_numbers;
};

INSTANTIATE_TEST_SUITE_P(FiveSeeds, RandomSequenceNumbers,
                         ::testing::Values(0, 1, 2, 3, 4)  // seed
);


/**
 * The lead can be interpreted as the position of a message relative to the newest message.
 * To test this property, this test inserts messages into a data structure at the position relative
 * to the current curser position. Afterwars we check if the messages were ordered.
 */
TEST_P(RandomSequenceNumbers, test_ordering_incoming_messages_based_on_lead) {  // NOLINT
  // GIVEN
  auto extractor = pulicast::core::LeadExtractor();

  auto cursor = int{0};
  auto received_messages_by_pos = std::map<int, int>{};
  auto store_in_messages_packets_based_on_lead =
      [&](uint32_t id, int lead) {
        received_messages_by_pos[cursor + lead] = id;
        if (lead >= 0) {
          cursor += lead;
        }
      };

  // WHEN
  for (const auto &seq_no : sequence_numbers) {
    int lead = extractor.ComputeLeadAndUpdateLastSequenceNumber({0, 0}, seq_no);
    store_in_messages_packets_based_on_lead(seq_no, lead);
  }

  // THEN
  auto sent_seq_no_set = std::set<int>(sequence_numbers.begin(), sequence_numbers.end());
  auto received_seq_no_set = std::set<int>();

  std::transform(std::begin(received_messages_by_pos), std::end(received_messages_by_pos),
                 std::inserter(received_seq_no_set, received_seq_no_set.begin()),
                 [](auto const &pair) {
                   return pair.second;
                 });

  EXPECT_THAT(received_seq_no_set, ::testing::ContainerEq(sent_seq_no_set));
  EXPECT_TRUE(is_ordered_seq_no_sequence(received_messages_by_pos));
}

class BurstOfLostMessages : public testing::TestWithParam<std::tuple<int, size_t> > {
 protected:
  void SetUp() override {
    seq_no_series[{0, 0}] = MakeSeqNumberSeriesWithBurstOfLostMessages(
        IdxOfMessageLossStart(), NumberOfLostMessages());
  }

  int IdxOfMessageLossStart() { return std::get<0>(GetParam()); }
  int NumberOfLostMessages() { return std::get<1>(GetParam()); }
  SequenceNumberSeries seq_no_series;
};

INSTANTIATE_TEST_SUITE_P(NonzeroMessagesLost, BurstOfLostMessages, ::testing::Combine(
    ::testing::Values(1, 5, 30, 50),  // idx of message loss start
    ::testing::Values(1, 3, 5, 10)));  // number of lost messages

TEST_P(BurstOfLostMessages, first_non_lost_message_should_have_lead_equivalent_to_number_of_lost_messages_plus_one) {  // NOLINT
  // GIVEN
  auto extractor = pulicast::core::LeadExtractor();

  // WHEN
  auto leads = ConsumeSeqNoSequences(seq_no_series, extractor);

  // THEN
  if (IdxOfMessageLossStart() > 0) {
    // We expect all leads to be one except for when the first message is lost, then the number of lost messages + 1.
    auto expected_leads = std::vector<int>(leads.size(), 1);
    expected_leads[IdxOfMessageLossStart()] = NumberOfLostMessages() + 1;

    EXPECT_THAT(leads, ::testing::ContainerEq(expected_leads));
  }
}