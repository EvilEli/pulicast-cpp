/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <pulicast/core/subscription_dispatcher.h>

TEST(TestSubscriptionDispatcher, dispatch_without_subscription) {  // NOLINT
  // GIVEN
  auto dispatcher = pulicast::core::SubscriptionDispatcher();
  auto data = std::array<char, 0>();

  // WHEN
  dispatcher.Dispatch(asio::buffer(data), {}, 0);

  // THEN
  // nothing crashes
  EXPECT_TRUE(true);
}

TEST(TestSubscriptionDispatcher, subscriptions_are_dispatched) {  // NOLINT
  // GIVEN
  auto dispatcher = pulicast::core::SubscriptionDispatcher();
  // misc
  auto message_info = pulicast::MessageInfo{pulicast::Address{1234, 1234}, 1};
  asio::const_buffer buffer(0, 0);

  // mock subscriptions
  using MockSubscription =
      ::testing::MockFunction<void(const char *,size_t, const pulicast::MessageInfo &)>;
  MockSubscription mock_handler1;
  MockSubscription mock_handler2;

  // THEN
  EXPECT_CALL(mock_handler1, Call(0, 0, message_info)).Times(1);
  EXPECT_CALL(mock_handler2, Call(0, 0, message_info)).Times(1);


  // WHEN
  dispatcher.Subscribe(mock_handler1.AsStdFunction());
  dispatcher.Subscribe(mock_handler2.AsStdFunction());
  dispatcher.Dispatch(buffer, message_info.source, message_info.lead);
}